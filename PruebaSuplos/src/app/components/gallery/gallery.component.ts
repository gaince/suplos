import { Component, OnInit } from '@angular/core';  
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { GalleryService } from 'src/app/services/gallery.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  // variables para obtener la data del api y visualizar el detalle de carga imagen
  public listCategory = [{key: "science", value: "Science"},{key: "education", value: "Education"},{key: "people", value: "People"}
,{key: "feelings", value: "Feelings"},{key: "computer", value: "Computer"},{key: "buildings", value: "Buildings"}]
  public palabra = ""; 
  public categoriaSeleccionada = "";
  public dataApi: any;
  public dataPreview = {
    webformatURL : null,
    webformatWidth : null,
    webformatHeight : null,
    views : null,
    tags : null,
    likes : null
  };
  
  palabraDigitada : FormGroup;
  //Realizamos el llamado del servicio para realizar las diferentes peticiones
  constructor(private _galleryService: GalleryService, private fb: FormBuilder) {
    //se realiza la intancia de un formulario reactivo para capturar el texto diligenciado en la caja
    this.palabraDigitada = fb.group({
      texto: new FormControl("")
    })
   }

  ngOnInit(): void {
    this.GetAllPhoto();
  }
  //Obtiene toda la informacion de la url base
  GetAllPhoto(){
    this._galleryService.GetAllPhoto().subscribe((response:any)=>{ 
      this.dataApi = response.hits;
    });
  }
  // Obtiene la información de las imagenes solo cuando se filtra por categoria
  GetFilterOnlyCategory(category:string){ 
    this._galleryService.GetFilterOnlyCategory(category).subscribe((response:any)=>{  
      this.dataApi = null;
      this.dataApi = response.hits;
    });
  }
// Obtiene la información de las imagenes solo cuando se filtra por un parametro digitado en la caja de texto
  GetFilterParameter(parameter:string){
    this._galleryService.GetFilterParameter(parameter).subscribe((response:any)=>{
      this.dataApi = null; 
      this.dataApi = response.hits;
    });
  }
// Obtiene la información de las imagenes cuando se filtra por categoria y se filtra un parametro
  GetFilterCategoryAndParameter(category:string,parameter:string){
    this._galleryService.GetFilterCategoryAndParameter(category,parameter).subscribe((response:any)=>{ 
      this.dataApi = null;
      this.dataApi = response.hits;
    });
  }
  // Obtiene el detalle de las imagenes
  GetDetailImage(data:any){  
    this.dataPreview = {
      webformatURL : data.webformatURL,
      webformatWidth : data.webformatWidth,
      webformatHeight : data.webformatHeight,
      views : data.views,
      tags : data.tags,
      likes : data.likes
    };
  }

  // valida el tipo de accion realizada
  TypeAction(){ 
    if(this.categoriaSeleccionada != "" && this.palabra != ""){
      this.GetFilterCategoryAndParameter(this.categoriaSeleccionada,this.palabra);
    }
    else if(this.categoriaSeleccionada != ""){
        this.GetFilterOnlyCategory(this.categoriaSeleccionada);
    }
    else if(this.palabra != ""){
      this.GetFilterParameter(this.palabra);
    }else{
      this.GetAllPhoto();
    }
  }
  // se obtiene el dato seleccionado del listado de categorias
  SelectCategory(data:any){ 
    this.categoriaSeleccionada = data.target.value;
    this.TypeAction();
  }
  //obtiene el texto diligenciado en el input
  ParameterInputText(){  
    this.palabra = this.palabraDigitada.get("texto")?.value;
    this.TypeAction();
  }
}
