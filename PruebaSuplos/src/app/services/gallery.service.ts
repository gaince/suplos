import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class GalleryService {
    private UrlApi = "https://pixabay.com/api/?key=13119377-fc7e10c6305a7de49da6ecb25";
        
    constructor(private _http: HttpClient){

    }

    GetAllPhoto(){
        return this._http.get(`${this.UrlApi}`);
    }

    GetFilterOnlyCategory(category:string){
        return this._http.get(`${this.UrlApi}&category=${category}`);
    }

    GetFilterParameter(parameter:string){
        return this._http.get(`${this.UrlApi}&lang=es&q=${parameter}`);
    }

    GetFilterCategoryAndParameter(category:string,parameter:string){
        return this._http.get(`${this.UrlApi}&category=${category}&lang=es&q=${parameter}`);
    }
}